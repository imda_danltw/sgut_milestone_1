import speech_recognition as sr
import pyttsx3
import datetime
import wikipedia
import webbrowser
import os
import time
import subprocess
from ecapture import ecapture as ec
import wolframalpha
import json
import requests
import spotipy
import sys
import spotipy.util as util
from json.decoder import JSONDecodeError
from spotipy.oauth2 import SpotifyClientCredentials
import credentials

engine = pyttsx3.init('espeak')
voices = engine.getProperty('voices')
engine.setProperty('voice', 'english+f4')
engine.setProperty('rate', 150)

username = sys.argv[1]
scope = 'user-read-private user-read-playback-state user-modify-playback-state'

cid = credentials.cid
secret = credentials.secret

client_credentials_manager = SpotifyClientCredentials(client_id = cid, client_secret = secret)
sp = spotipy.Spotify(client_credentials_manager = client_credentials_manager)

def speak(text):
    engine.say(text)
    engine.runAndWait()

def greetUser():
    hour = datetime.datetime.now().hour
    if hour >=0 and hour < 12:
        speak("Hi! Good Morning! How may I help you?")
        print("Hi! Good Morning! How may I help you?")

    elif hour >= 12 and hour < 18:
        speak("Hi! Good Afternoon! How may I help you?")
        print("Hi! Good Afternoon! How may I help you?")

    else:
        speak("Hi! Good Evening! How may I help you?")
        print("Hi! Good Evening! How may I help you?")

def takeCmd():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening.....")
        audio = r.listen(source)

        try:
            stmt = r.recognize_google(audio, language='en-US')
            print(f"You said:{stmt}\n")

        except Exception as e:
            speak("I'm Sorry, Please repeat your request")
            return "None"

        return stmt

speak("Loading your AI PA... Please wait...")
print("Loading your AI PA... Please wait...")
greetUser()

if __name__ == '__main__':

    while True:
        stmt = takeCmd().lower()
        if stmt == 0:
            continue

        if "good bye" in stmt or "ok bye" in stmt or "stop" in stmt or "thank you" in stmt:
            speak("Thank you for using AI PA, Goodbye")
            print("Thank you for using AI PA, Goodbye")
            break

        if "wikipedia" in stmt:
            speak('Please wait while I search Wikipedia')
            stmt = stmt.replace("wikipedia", "")
            result = wikipedia.summary(stmt, sentences=3)
            speak("I have searched Wikipedia and fount the following")
            print(result)
            speak(result)

        elif 'open youtube' in stmt:
            webbrowser.open_new_tab("https://www.youtube.com")
            speak("I have opened youtube for you")
            time.sleep(5)

        elif 'open google' in stmt:
            webbrowser.open_new_tab("https://www.google.com.sg")
            speak("I have opened google for you")
            time.sleep(5)

        elif 'open gmail' in stmt:
            webbrowser.open_new_tab("https://www.gmail.com")
            speak("I have opened gmail for you")
            time.sleep(5)

        elif 'time' in stmt:
            strtime = datetime.datetime.now().strftime("%H:%M:%S")
            speak(f"the time now is {strtime}")

        elif 'news' in stmt:
            news = webbrowser.open_new_tab("https://www.channelnewsasia.com/")
            speak("Here's the latest news from channel news asia")
            time.sleep(6)

        elif 'covid' in stmt:
            news = webbrowser.open_new_tab("https://www.channelnewsasia.com/news/topics/coronavirus-covid-19?cid=cna-sghome_covid19_outbreak_desktop_11032020_cna")
            speak("Here's the latest covid news from channel news asia")
            time.sleep(6)

        elif "camera" in stmt or "take a photo" in stmt:
            ec.capture(0, "robo camera", "img.jpg")

        elif 'search' in stmt:
            stmt = stmt.replace("search", "")
            webbrowser.open_new_tab(stmt)
            time.sleep(5)

        elif 'ask' in stmt:
            speak('I can answer to computational and geographical questions  and what question do you want to ask now')
            question = takeCmd()
            app_id = credentials.wolframalphakey
            client = wolframalpha.Client(app_id)
            res = client.query(question)
            answer = next(res.results).text
            speak(answer)
            print(answer)

        elif "weather" in stmt:
            api_key = credentials.openweathermapkey
            base_url = "https://api.openweathermap.org/data/2.5/weather?"
            speak("whats the city name")
            city_name = stmt()
            complete_url = base_url + "appid=" + api_key + "&q=" + city_name
            response = requests.get(complete_url)
            x = response.json()
            if x["cod"] != "404":
                y = x["main"]
                current_temperature = float(y["temp"]) - 273.15
                current_humidity = y["humidity"]
                z = x["weather"]
                weather_description = z[0]["description"]
                speak(" Temperature in celsius is " +
                      str(current_temperature) +
                      "\n humidity in percentage is " +
                      str(current_humidity) +
                      "\n description  " +
                      str(weather_description))
                print(" Temperature in celsius unit = " +
                      str(current_temperature) +
                      "\n humidity (in percentage) = " +
                      str(current_humidity) +
                      "\n description = " +
                      str(weather_description))

            else:
                speak("City Not Found")

time.sleep(3)